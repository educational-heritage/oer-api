/**
 * Test the OER API
 */

'use strict'

module.exports = {
  suites: [{
    name: 'OER API US007: Create a new resource',
    tests: [
      {
        name: 'authentication_register',
        description: 'Users should be registered',
        api_calls: [
          {
            it: 'Alice can be registered',
            request: 'POST /authentication/sign_in',
            params: { email: '{{alice.email}}', password: '{{alice.password}}' },
            status: 200,
            save: { 'alice.token': 'body.result.token' }
          }
        ]  // api_calls
      },
      {
        name: 'can_do',
        description: 'What someone can do',
        api_calls: [
          {
            it: 'Alice can create a new resource',
            request: {
              method: 'POST',
              path: '/persistence/resources',
              params: { title: 'Alice resource 0 US007' },
              files: { attachment: './mocks/148x210.png' }
            },
            headers: { Authorization: 'Bearer ' + '{{alice.token}}' },
            status: 201,
            save: { 'alice.resources.0': 'body.result._id' }
          },
          {
            it: 'Alice can view her new resource details',
            request: 'GET /persistence/resources/' + '{{alice.resources.0}}',
            headers: { Authorization: 'Bearer ' + '{{alice.token}}' },
            status: 200,
            save: { 'alice.attachments.0': 'body.result.attachment' }
          },
          {
            it: 'Alice can view her new resource attachment',
            request: 'GET /attachments/' + '{{alice.attachments.0}}',
            headers: { Authorization: 'Bearer ' + '{{alice.token}}' },
            status: 200
          }
        ]  // api_calls
      },
      {
        name: 'can_not_do',
        description: 'What anyone can not do',
        api_calls: [
          {
            it: 'Anonymous users can not create a resource',
            request: 'POST /persistence/resources',
            params: { title: 'Anonimous resource 0 US007' },
            status: 401
          }
        ]  // api_calls
      }
    ]  // tests
  }]  // suites
}  // module.exports
