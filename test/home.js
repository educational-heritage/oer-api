/**
 * Test the OER API
 */

'use strict'

module.exports = {
  suites: [{
    name: 'OER API: JSON home',
    tests: [
      {
        name: 'get_home',
        description: 'Should get JSON home',
        api_calls: [{
          it: 'As a user I could see API title and version in home page',
          request: {
            method: 'GET',
            path: '/'
          },
          status: 200,
          assert: [{
            select: 'body.api',
            equal_keys: {
              title: 'Educational Heritage API'
            }
          }]
        }]
      }
    ]  // tests
  }]  // suites
}  // module.exports
