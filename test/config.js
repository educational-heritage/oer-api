'use strict'

const config = require('config')

module.exports = {
  config: {
    defaults: {
      api_call: {
        request: {
          base_url: config.get('api.url')
        }
      }
    }
  }
}
