/**
 * Test the OER API
 */

'use strict'

module.exports = {
  suites: [{
    name: 'OER API US005: Save a resource as favourite',
    tests: [
      {
        name: 'authentication_register',
        description: 'Users should be registered',
        api_calls: [
          {
            it: 'Alice can be registered',
            request: 'POST /authentication/register',
            params: { email: '{{alice.email}}', password: '{{alice.password}}' },
            status: 201,
            save: { 'alice.token': 'body.result.token' }
          },
          {
            it: 'Bob can be registered',
            request: 'POST /authentication/register',
            params: { email: '{{bob.email}}', password: '{{bob.password}}' },
            status: 201,
            save: { 'bob.token': 'body.result.token' }
          }
        ]  // api_calls
      },
      {
        name: 'post_resources',
        description: 'There should be some resources',
        api_calls: [
          {
            it: 'Alice can create a resource',
            request: 'POST /persistence/resources',
            params: { title: 'Alice resource 0 US005' },
            headers: { Authorization: 'Bearer ' + '{{alice.token}}' },
            status: 201,
            save: { 'alice.resources.0': 'body.result._id' }
          },
          {
            it: 'Alice can create another resource',
            request: 'POST /persistence/resources',
            params: { title: 'Alice resource 1 US005' },
            headers: { Authorization: 'Bearer ' + '{{alice.token}}' },
            status: 201,
            save: { 'alice.resources.1': 'body.result._id' }
          },
          {
            it: 'Bob can create a resource',
            request: 'POST /persistence/resources',
            params: { title: 'Bob resource 0 US005' },
            headers: { Authorization: 'Bearer ' + '{{bob.token}}' },
            status: 201,
            save: { 'bob.resources.0': 'body.result._id' }
          },
          {
            it: 'Bob can create another resource',
            request: 'POST /persistence/resources',
            params: { title: 'Bob resource 1 US005' },
            headers: { Authorization: 'Bearer ' + '{{bob.token}}' },
            status: 201,
            save: { 'bob.resources.1': 'body.result._id' }
          }
        ]  // api_calls
      },
      {
        name: 'authentication_anonymous',
        description: 'As an anonymous user I could not save resources as favourites',
        api_calls: [
          {
            it: 'Non authenticated user could not save a resource as favourite',
            request: 'POST /relationship',
            params: { route: 'favourite', hey: 'anonymous@example.com', foreign: 'resources', value: 'any_value' },
            status: 401,
            assert: { equal: 'Unauthorized' }
          }
        ]  // api_calls
      },
      {
        name: 'can_do',
        description: 'What someone can do as an authenticated user',
        api_calls: [
          {
            it: 'Alice could save her own resources as favourites',
            request: 'POST /relationship',
            params: { route: 'favourite', key: '{{alice.email}}', foreign: 'resources', value: '{{alice.resources.0}}' },
            headers: { Authorization: 'Bearer ' + '{{alice.token}}' },
            status: 201,
            assert: { select: 'body.result', size: 1 }
          },
          {
            it: 'Alice could save Bob resources as her own favourites',
            request: 'POST /relationship',
            params: { route: 'favourite', key: '{{alice.email}}', foreign: 'resources', value: '{{bob.resources.0}}' },
            headers: { Authorization: 'Bearer ' + '{{alice.token}}' },
            status: 201,
            assert: { select: 'body.result', size: 1 }
          },
          {
            it: 'Alice could view all her saved as favourites resources',
            request: 'GET /relationship/favourite/{{alice.email}}/resources',
            headers: { Authorization: 'Bearer ' + '{{alice.token}}' },
            status: 200,
            assert: {
              // select: 'body.details',
              select: 'body.result',
              size: 2,
              equal: ['{{alice.resources.0}}', '{{bob.resources.0}}']
            }
          }
        ]  // api_calls
      },
      {
        name: 'can_not_do',
        description: 'What anyone can not do as an authenticated user',
        api_calls: [
          {
            it: 'Bob could not view Alice favourites resources',
            request: 'GET /relationship/favourite/{{alice.email}}/resources',
            headers: { Authorization: 'Bearer ' + '{{bob.token}}' },
            status: 401,
            assert: { equal: 'Unauthorized' }
          },
          {
            it: 'Bob could not save any resource as Alice favourites'
            // request: 'POST /relationship',
            // params: { route: 'favourite', key: '{{alice.email}}', foreign: 'resources', value: '{{bob.resources.1}}' },
            // headers: { Authorization: 'Bearer ' + '{{bob.token}}' },
            // status: 401,
            // assert: { equal: 'Unauthorized' }
          },
          {
            it: 'Alice could not save the same resource as favourite twice',
            request: 'POST /relationship',
            params: { route: 'favourite', key: '{{alice.email}}', foreign: 'resources', value: '{{alice.resources.0}}' },
            headers: { Authorization: 'Bearer ' + '{{alice.token}}' },
            status: 409
          }
        ]  // api_calls
      }
    ]  // tests
  }]  // suites
}  // module.exports
