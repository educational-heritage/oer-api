'use strict'

module.exports = {
  data: {
    alice: {
      email: 'alice.{{$run_id}}@example.com',
      password: 'secret',
      name: 'Alice Name',
      token: '',
      resources: [],
      attachments: []
    },
    bob: {
      email: 'bob.{{$run_id}}@example.com',
      password: 'other_secret',
      name: 'Bob Name',
      token: '',
      resources: []
    }
  }
}
