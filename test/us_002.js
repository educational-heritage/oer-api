/**
 * Test the OER API
 */

'use strict'

module.exports = {
  suites: [{
    name: 'OER API US002: View resource details',
    tests: [
      {
        name: 'authentication_register',
        description: 'User should be registered',
        api_calls: [{
          request: {
            method: 'POST',
            path: '/authentication/register',
            params: { email: 'alice.{{$api_call_id}}@example.com', password: '{{alice.password}}' }
          },
          status: 201,
          save: { 'alice.token': 'body.result.token' }
        }]
      },
      {
        name: 'post_resource',
        description: 'There should be some resources',
        api_calls: [{
          request: {
            method: 'POST',
            path: '/persistence/resources',
            params: { title: 'Alice resource 0', author: 'alice name' },
            headers: { Authorization: 'Bearer ' + '{{alice.token}}' }
          },
          status: 201,
          save: { 'alice.resources.0': 'body.result._id' }
        }]
      },
      {
        name: 'get_resource',
        description: 'Fetch info about a resource',
        api_calls: [
          {
            request: {
              method: 'POST',
              path: '/search/resources',
              params: { title_slug: 'Alice-resource-0', author_slug: 'alice-name' },
              headers: { Authorization: 'Bearer ' + '{{alice.token}}' }
            },
            status: 200,
            assert: [{
              select: 'body',
              equal_keys: {
                message: 'OK'
              }
            }, {
              select: 'body.details',
              size: 1
            }, {
              select: 'body.details.0',
              equal_keys: {
                title: 'Alice resource 0'
              }
            }]
          },
          {
            it: 'No results with empty query'
          }
        ]  // api_calls
      }
    ]  // tests
  }]  // suites
}  // module.exports
