/**
 * Test the OER API
 */

'use strict'

let today = new Date()
let yesterday = new Date()
let beforeYesterday = new Date()
let tomorrow = new Date()

yesterday.setDate(today.getDate() - 1)
beforeYesterday.setDate(today.getDate() - 2)
tomorrow.setDate(today.getDate() + 1)

module.exports = {
  suites: [{
    name: 'OER API US001: View all resources',
    tests: [
      {
        name: 'authentication_register',
        description: 'User should be registered',
        api_calls: [{
          request: {
            method: 'POST',
            path: '/authentication/register',
            params: { email: 'alice.{{$api_call_id}}@example.com', password: '{{alice.password}}' }
          },
          status: 201,
          save: { 'alice.token': 'body.result.token' }
        }]
      },
      {
        name: 'post_asset',
        description: 'There should be some resources',
        api_calls: [
          {
            request: {
              method: 'POST',
              path: '/persistence/resources',
              params: {
                title: 'Alice asset 0 US001',
                published: 'false',
                publishedDate: tomorrow },
              headers: { Authorization: 'Bearer ' + '{{alice.token}}' }
            },
            status: 201,
            save: { 'alice.resources.0': 'body.result._id' }
          },
          {
            request: {
              method: 'POST',
              path: '/persistence/resources',
              params: {
                title: 'Alice asset 1 US001',
                published: 'true',
                publishedDate: yesterday },
              headers: { Authorization: 'Bearer ' + '{{alice.token}}' }
            },
            status: 201,
            save: { 'alice.resources.1': 'body.result._id' }
          },
          {
            request: {
              method: 'POST',
              path: '/persistence/resources',
              params: {
                title: 'Alice asset 2 US001',
                published: 'true',
                publishedDate: beforeYesterday },
              headers: { Authorization: 'Bearer ' + '{{alice.token}}' }
            },
            status: 201,
            save: { 'alice.resources.2': 'body.result._id' }
          }
        ]  // api_calls
      },
      {
        name: 'get_asset',
        description: 'Fetch all assets',
        api_calls: [
          {
            request: {
              method: 'GET',
              path: '/persistence/resources?sort=publishedDate'
            },
            status: 200,
            assert: [
              { select: 'body.result.1', equal_keys: { title: 'Alice asset 1 US001' } },
              { select: 'body.result.0', equal_keys: { title: 'Alice asset 2 US001' } }
            ]
          },
          {
            it: 'No results with empty query'
          }
        ]  // api_calls
      }
    ]  // tests
  }]  // suites
}  // module.exports
