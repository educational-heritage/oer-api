module.exports = {
  api: {
    filters: {
      resources: {
        published: 'true',
        publishedDate: {$lte: new Date().toISOString()}
      }
    }
  }
}
