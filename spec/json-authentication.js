// List of allowed public routes

module.exports = {
  api: {
    authentication: {
      equal_to: [
        { path: '/' }
      ],
      start_by: [
        { path: '/authentication/register' },
        { path: '/authentication/sign_in' },
        { path: '/persistence', method: 'GET' },
        { path: '/search', method: 'POST' }
      ],
      is_owner: [
        { path: '/relationship/favourite/:user' }
      ]
    }
  }
}
