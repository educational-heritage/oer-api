module.exports = {
  api: {
    validations: {
      resources: {
        title: [ 'unique', 'required' ]
      },
      register: {
        email: [ 'unique', 'required' ],
        password: [ 'required' ]
      }
    }
  }
}
