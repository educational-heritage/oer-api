const defer = require('config/defer').deferConfig
const path = require('path')

module.exports = {
  yabaas: {
//    persistence: 'in_memory',
    persistence: 'mongo',
    upload: path.join(process.cwd(), '/public')
  },
  mongo: {
    host: '127.0.0.1',
    port: '6793',
    url: defer(cfg => 'mongodb://' + cfg.mongo.host + ':' + cfg.mongo.port)
  },
  api: {
    host: '127.0.0.1',
    port: '6792',
    url: defer(cfg => 'http://' + cfg.api.host + ':' + cfg.api.port)
  }
  // rabbit: {
  //   host: 'oer_rabbit',
  //   port: '5672',
  //   user: 'admin',
  //   pass: 'pass'
  // },
  // smtp: {
  //   host: 'oer_maildev',
  //   port: '25',
  //   secure: false,
  //   from: 'no-reply@yabaas.org'
  // }
}
