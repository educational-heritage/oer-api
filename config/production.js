module.exports = {
  mongo: {
    host: 'oer_mongo',
    port: '27017'
  },
  api: {
    host: 'oer_api',
    port: '3000'
  }
}
