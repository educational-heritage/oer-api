module.exports = {
  mongo: {
    host: 'mongo',
    port: '27017'
  },
  api: {
    host: 'sergioalonso__api',
    port: '3000'
  }
}
