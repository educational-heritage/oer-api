module.exports = {
  mongo: {
    host: 'oer_mongo',
    port: '27017'
  },
  api: {
    host: 'oer_yabaas',
    port: '3000'
  }
}
