FROM node:8.5.0

WORKDIR /usr/src/app

COPY package.json .
COPY package-lock.json .

ENV PATH /usr/src/app/node_modules/.bin:$PATH

RUN npm install --silent --ignore-scripts --progress=false --unsafe-perm

COPY . /usr/src/app
