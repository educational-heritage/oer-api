#!/usr/bin/env node

const debug = require('debug')('oer-api:populate')

const faker = require('faker/locale/es')
const request = require('request')

let token = ''

request.post(
    'http://127.0.0.1:6792/authentication/register',
    {form: {email: 'user@example.com', password: 'secret'}},
    (error, response, body) => {
      if (error) { debug('error: %o', error) }
      debug('response: %s', response.statusCode)
      debug('body: %o', response.body)
      token = response.token
    }
)

request.post(
    'http://127.0.0.1:6792/authentication/sign_in',
    {form: {email: 'user@example.com', password: 'secret'}},
    (error, response, body) => {
      if (error) { debug('error: %o', error) }
      debug('response: %s', response.statusCode)
      debug('body: %o', JSON.parse(body).result.token)
      token = JSON.parse(body).result.token

      for (var i = 0; i < 10; i++) {
        const author = faker.name.firstName() + ' ' + faker.name.lastName()
        const authorSlug = author.toLowerCase().replace(/\s+/g, '-')
        const title = faker.lorem.sentence()
        const titleSlug = faker.lorem.slug()
        const phoneMedia = '//placehold.it/169x240'
        const tabletMedia = '//placehold.it/294x360'
        const desktopMedia = '//placehold.it/296x420'
        const defaultMedia = '//placehold.it/159x210'
        const thumbnail = '//placehold.it/148x210'
        const content = faker.lorem.paragraphs()
        const detailUrl = '/' + authorSlug + '/' + titleSlug
        const published = faker.random.boolean().toString()
        const publishedDate = faker.date.past()

        request.post(
          'http://127.0.0.1:6792/persistence/resources',
          {
            form: {
              author: author,
              authorSlug: authorSlug,
              title: title,
              titleSlug: titleSlug,
              phoneMedia: phoneMedia,
              tabletMedia: tabletMedia,
              desktopMedia: desktopMedia,
              defaultMedia: defaultMedia,
              thumbnail: thumbnail,
              content: content,
              detailUrl: detailUrl,
              published: published,
              publishedDate: publishedDate
            },
            'auth': { 'bearer': token }
          },
          function (error, response, body) {
            if (error) { return debug('error: %o', error) }
            debug('response: %s', response.statusCode)
            debug('body: %o', response.body)
          }
      )
      }
    })
